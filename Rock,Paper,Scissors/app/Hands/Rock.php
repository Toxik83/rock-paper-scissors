<?php
namespace Hands;
class Rock {
    private $name = "Rock";
    private $beats = "Scissors";
    
     public function getBeats() {
      return $this->beats;
    }
    public function getName() {
      return $this->name;
    }
}
