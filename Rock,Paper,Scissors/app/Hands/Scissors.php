<?php
namespace Hands;
class Scissors {
  private $name = "Scissors";  
  private $beats = "Paper";
  
   public function getBeats() {
      return $this->beats;
    }
    public function getName() {
      return $this->name;
    }
}