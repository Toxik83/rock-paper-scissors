<?php

namespace Game;

class Game {

    private $rounds;
    private $player_rounds;
    private $playerTwo_rounds;

    public function playRounds($newRounds) {
        $this->rounds = $newRounds;
    }

    protected function winner() {
        if ($this->player_rounds > $this->playerTwo_rounds) {
            echo "Winner is Player";
        } else if ($this->player_rounds < $this->playerTwo_rounds) {
            echo "Winner is Player Two";
        } else {
            echo "Tie";
        }
    }
   
    public function play($hands) {
      if(is_array($hands)){
        while ($this->rounds > 0) {
            echo "Round $this->rounds";
            $players = array_rand($hands, 2);
            $player = $hands[$players[0]];
            $playerTwo = $hands[$players[1]]; 
            if ($player->getName() === $playerTwo->getName()) {
                continue;
            }else if ($player->getBeats() === $playerTwo->getName()) {
                echo "<br>"."Player  wins"."<br>";
                $this->player_rounds++;
            }else if ($playerTwo->getBeats() === $player->getName()) {
                echo "<br>"."Player  two wins"."<br>";
                $this->playerTwo_rounds++;
            }
            $this->rounds--;
        }
        }
        $this->winner();
    }
}
