<?php
require_once 'start.php';
use Hands\Rock;
use Hands\Paper;
use Hands\Scissors;
use Game\Game;
$rock = new Rock();
$paper = new Paper();
$scissors = new Scissors();
$hands = array($rock, $paper, $scissors);

$game = new Game();
$game->playRounds(3);
$game->play($hands);
?>
